//This green text is to tell you what in the name of
//fresh fuck is this spaghetti.

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

//NOTE: Some of these MIGHT be redundant and unused.
//I'm just too lazy to figure out which.
number1: number;
number2: number;
answer: number;
displayResult: string;
type: string;
combo: number;
comboText: string;
highestcombo: number;
corrects: number;
wrongs: number;

  constructor(private router: Router, private storage: Storage) {}

  ngOnInit() {
    //These three bastards read the value from the local storage
    //for the statistics page where you can see your total
    //correct answers, wrong answers and your highest combo.
    this.storage.get('corrects').then((val) => {
    this.corrects = JSON.parse(val);
    if (!this.corrects) {
      this.corrects = 0;
    }
    });

    this.storage.get('wrongs').then((val) => {
    this.wrongs = JSON.parse(val);
    if (!this.wrongs) {
      this.wrongs = 0;
    }
    });

    this.storage.get('highestcombo').then((val) => {
    this.highestcombo = JSON.parse(val);
    if (!this.highestcombo) {
      this.highestcombo = 0;
    }
    });

    //These set the playfield to the default values.

    this.number1 = Math.floor((Math.random() * 10) + 1);
    this.number2 = Math.floor((Math.random() * 10) + 1);
    this.type = "+";
    this.combo = 0
    this.displayResult = "Take a guess...";
  }

//Click the Stats icon in the header to access stats.
show_stats() {
  this.router.navigateByUrl('stats');
}

//Click the Skull icon in the header to find out the idiot
//behind this mess.
show_credits() {
  this.router.navigateByUrl('credits');
}
//Rerolls another set of numbers for you to think on.
reset() {
  this.number1 = Math.floor((Math.random() * 10) + 1);
  this.number2 = Math.floor((Math.random() * 10) + 1);
}
//If the answer is correct, add a point to Correct answers.
correct() {
  this.displayResult = "Correct!";
  this.corrects += 1;
  this.combo += 1;
  this.storage.set('corrects', JSON.stringify(this.corrects));
}
//If the answer is wrong (you idiot lmao), add a point to Wrong answers.
wrong() {
  this.displayResult = "Wrong, you idiot. Come on, step it up now!";
  this.wrongs += 1;
  this.storage.set('wrongs', JSON.stringify(this.wrongs));

  //But if your current combo is higher than your previous best,
  //this saves your new record.
  if (this.combo > this.highestcombo) {
    this.highestcombo = this.combo;
    this.storage.set('highestcombo', JSON.stringify(this.highestcombo));
  }
  this.combo = 0;
}
//Calculates the right answer and compares it to yours.
calc2() {
  if (this.type === "+") {
    if (this. answer === this.number1 + this.number2) {
      this.correct();
    } else {
      this.wrong();
    }
  } else if (this.type === "-") {
    if (this. answer === this.number1 - this.number2) {
      this.correct();
    } else {
      this.wrong();
    }
  } else if (this. type === "x") {
    if (this. answer === this.number1 * this.number2) {
      this.correct();
    } else {
      this.wrong();
    }
  }
  this.reset();
  this.answer = null
  
}
}