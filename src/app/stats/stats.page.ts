import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-stats',
  templateUrl: './stats.page.html',
  styleUrls: ['./stats.page.scss'],
})
export class StatsPage implements OnInit {
  highestcombo: number;
  corrects: number;
  wrongs: number;
  
  constructor(private storage: Storage) { }

  ngOnInit() {

    this.storage.get('corrects').then((val) => {
      this.corrects = JSON.parse(val);
      if (!this.corrects) {
        this.corrects = 0;
      }
      });
  
      this.storage.get('wrongs').then((val) => {
        this.wrongs = JSON.parse(val);
        if (!this.wrongs) {
          this.wrongs = 0;
        }
        });
  
        this.storage.get('highestcombo').then((val) => {
          this.highestcombo = JSON.parse(val);
          if (!this.highestcombo) {
            this.highestcombo = 0;
          }
          });
  
  }

}
